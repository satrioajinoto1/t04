class readystock extends kue{
    private double jumlah;

    // digunakan untuk mengakses parent class kue
    public readystock(String name, double price, int stock){
        super(name, price, stock);
    }
     // method berat di return 0 karena pada class ini yang dihitung adalah jumlah dari kue tersebut
    public double Berat(){
        return 0;
    }  
    // menggunakan stock sebagai berat kue
    public double Jumlah(){
        jumlah = getStock();
        return jumlah;
    }
    // menghitung harga kue dengan menggunakan method getPrice() yang berada di superclass dan menggunakan method Jumlah()
    public double hitungHarga(){
        return getPrice() * Jumlah() * 2;
    }

}