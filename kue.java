public abstract class kue{
    private String name;
    private double price; 
    // menambahkan stock agar dapat digunakan di kelas extends sebagai jumlah
    private int stock;
    public kue(String name, double price, int stock){
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }
    public void getPrice(double price) {
        this.price = price;
    }
    // menambahkan getStock()
    public int getStock(){
        return stock;
    }
    abstract public double hitungHarga();
    abstract public double Berat();
    abstract public double Jumlah();
    @Override
    public String toString(){
        return String.format("\n---------- "+getName()+" ----------"+"\nHarga\t\t:"+getPrice());
    }
}