class pesanan extends kue{
    private double berat;

    // digunakan untuk mengakses parent class kue
    public pesanan(String name, double price, int stock){
        super(name, price, stock);
    }
    // menggunakan stock sebagai berat kue
    public double Berat(){
        berat = getStock();
        return berat;
    }
    // menghitung harga kue dengan menggunakan method getPrice() yang berada di superclass dan menggunakan method Berat()
    public double hitungHarga(){
        return getPrice() * Berat();
    }
    // method jumlah di return 0 karena pada class ini yang dihitung adalah berat dari kue tersebut
    public double Jumlah(){
        return 0;
    }


}